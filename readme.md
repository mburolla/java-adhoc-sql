# Java Ad Hoc SQL
Illustrates how to read/write to Aurora DB using the MySqlConnector Jar file.

    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>8.0.26</version>
    </dependency>

