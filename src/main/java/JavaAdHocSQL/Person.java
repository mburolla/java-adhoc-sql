package JavaAdHocSQL;

public class Person {

    //
    // Data members
    //

    private int id;
    private String name;

    //
    // Constructors
    //

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    //
    // Accessors
    //

    public int getId() { return id; }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
