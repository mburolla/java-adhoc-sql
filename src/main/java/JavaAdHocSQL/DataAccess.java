package JavaAdHocSQL;

import java.sql.*;
import java.io.IOException;
import java.util.Properties;
import java.io.FileNotFoundException;

public class DataAccess {

    //
    // Data Members
    //

    private String url = "";
    private String pwd = "";
    private String username = "";
    private final String SELECT_PERSON = "select * from person where id = ?";
    private final String UPDATE_PERSON = "update person set name = ? where id = ?";

    //
    // Constructors
    //

    public DataAccess() {
        try {
            var classloader = Thread.currentThread().getContextClassLoader();
            var inputStream = classloader.getResourceAsStream("application.properties");
            var properties = new Properties();
            properties.load(inputStream);
            this.url = properties.getProperty("URL");
            this.pwd = properties.getProperty("PWD");
            this.username = properties.getProperty("USERNAME");
        }
        catch (FileNotFoundException e) {
            System.out.println(e);
        }
        catch (IOException ioe) {
            System.out.println(ioe);
        }
    }

    //
    // Public Methods
    //

    public Person getPerson(int personId) {
        Person retval = null;
        try {
            try (Connection conn = DriverManager.getConnection(this.url, this.username, this.pwd)) { // AutoClosable "try-with"
                var preparedStatement = conn.prepareStatement(this.SELECT_PERSON);
                preparedStatement.setInt(1, personId); // 1 based index.
                var resultSet = preparedStatement.executeQuery(); // Forward only data reader.
                resultSet.next(); // Must move to first result set.
                retval = new Person(resultSet.getInt("id"), resultSet.getString("name"));
            }
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return retval;
    }

    public void updatePersonName(int personId, String newName) {
        try {
            try (Connection conn = DriverManager.getConnection(this.url, this.username, this.pwd)) { // AutoClosable "try-with"
                var preparedStatement = conn.prepareStatement(this.UPDATE_PERSON);
                preparedStatement.setString(1, newName);
                preparedStatement.setInt(2, personId);
                preparedStatement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    //
    // Private Methods
    //

    private void printToConsole(ResultSet rs) {
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (rs.next()) {
                for (int i = 1; i <= columnsNumber; i++) {
                    if (i > 1) System.out.print(",  ");
                    String columnValue = rs.getString(i);
                    System.out.print(columnValue + " " + rsmd.getColumnName(i));
                }
                System.out.println("");
            }
        }
        catch(SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}