package JavaAdHocSQL;

public class Main {
    public static void main(String[] args) {
        var dataAccess = new DataAccess();
        var person= dataAccess.getPerson(1);
        dataAccess.updatePersonName(1, "Joey Jr.");
        System.out.println(person);
    }
}
